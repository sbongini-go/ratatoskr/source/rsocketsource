package rsocketsource

import (
	"context"

	"github.com/rsocket/rsocket-go/payload"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// createSpan Creo lo span partendo dal payload rsocket
func (c *rSocketSource) createSpan(ctx context.Context, msg *payload.Payload) (context.Context, trace.Span) {
	// TODO estrarre traccia precedente
	// TODO arricchire span
	ctx, span := otel.Tracer("").Start(ctx, "Run")

	span.SetAttributes(
		attribute.String("span.kind", trace.SpanKindServer.String()),
	)

	return ctx, span
}
