# RSocketSource

**Source** che accetta in input comunicazioni tramite protocollo *RSocket*.

Il protocollo *RSocket* ha 4 modalita di comunicazione:

* **Fire And Forgot**: Il client invia una richiesta al server ma non resta in attesa della risposta.
* **Request/Response**: Il client invia una richiesta al server e si aspetta una singola risposta.
* **Request Stream**: Il client invia una richiesta e il server potra inviare uno Stream di rispose, ovvero potra inviare più messaggi di risposta.
* **Channel**: Il client apre un *canale* verso il server. Sia il client che il server potranno inviarsi messaggi, questi ultimi potranno non essere correlati tra di loro.

Il *Source* **RSocketSource** funziona come server *RSocket* ed espone tutte e 4 le modalità di comunicazione, anche se ci sono alcuni importanti punti di attenzione dovuti alla architettura di *Ratatroskr*:

1. *Request Stream* e *Channel* si comportano come *Request Response* in quanto a ogni richiesta corrispondera una sola risposta.
2. *RSocketSource* non gestisce i metadati di RSocket

## TODO

1. Capire come gestire i metadati di *RSocket*

2. Capire come utilizzare i *SetupPayload*

3. Dare la possibilita di utilizzare altri protocolli di trasporto oltre TCP:

   Esempio:

   ```go
   rsocket.TCPClient().SetHostAndPort("127.0.0.1", 7878).Build()
   rsocket.WebsocketClient().SetURL("ws://127.0.0.1:8080/hello").Build()
   rsocket.UnixClient().SetPath("/var/run/rsocket.sock").Build()
   ```