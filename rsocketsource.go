package rsocketsource

import (
	"context"
	"fmt"
	"sync/atomic"

	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"gitlab.com/sbongini-go/ratatoskr/core/source"

	"github.com/rsocket/rsocket-go"
	"github.com/rsocket/rsocket-go/payload"
	"github.com/rsocket/rsocket-go/rx/flux"
	"github.com/rsocket/rsocket-go/rx/mono"
)

type rSocketSource struct {
	// embedded type contente campi e metodi coumin ai service
	source.CommonSource `mapstructure:",squash"`

	// TypeSource tipo di Source, sara sempre "kafka"
	TypeSource string `yaml:"type" mapstructure:"type"`

	port int

	// nextHandler stringa che indica quale handler dovra gestire l'evento generato da questo Source
	nextHandler string `yaml:"nexthandler" mapstructure:"nexthandler"`

	// ready impostata a 1 se il consumer e' ready impostata a 0 se non lo e'
	ready int32 `yaml:"-"`
}

// New instanzia un nuovo KafkaSource
func New(nextHandler string, opts ...Option) core.Source {
	c := rSocketSource{
		nextHandler: nextHandler,
		TypeSource:  "rsocket",
		port:        7878,
	}

	// Applico gli option patterns
	for _, opt := range opts {
		opt(&c)
	}

	return &c
}

// Init funzione chiamata dal Core per inizializzare il Source
func (c *rSocketSource) Init(ctx context.Context, initBag core.InitSourceBag) error {
	c.SetLogger(initBag.Logger)
	return nil
}

// Start avvia il source
func (c *rSocketSource) Start(ctx context.Context, bridge core.Bridge) error {
	logger := c.Logger()

	// Canale su cui verra' notificato il fatto che il server RSocket e' stato avviato
	startedChan := make(chan error)

	s := rsocket.Receive().
		OnStart(func() {
			logger.Debug("rsocket server ready")
			// Imposto il flag che indica che il Source e' ready
			atomic.StoreInt32(&c.ready, 1)
			close(startedChan)
		}).
		Acceptor(func(ctx context.Context, setup payload.SetupPayload, sendingSocket rsocket.CloseableRSocket) (rsocket.RSocket, error) {
			// bind responder
			return rsocket.NewAbstractSocket(
				rsocket.MetadataPush(func(request payload.Payload) {
					// Creo lo span opentelemetry
					ctx, span := c.createSpan(ctx, &request)
					defer span.End()

					// Invio l'evento al prossimo Handler
					_, err := bridge.Next(ctx, c.nextHandler, model.Event{})
					if nil != err {
						logger.Val("error", err).Error("next handler error")
					}
				}),
				rsocket.FireAndForget(func(request payload.Payload) {
					// Creo lo span opentelemetry
					ctx, span := c.createSpan(ctx, &request)
					defer span.End()

					// Invio l'evento al prossimo Handler
					_, err := bridge.Next(ctx, c.nextHandler, createEvent(request))
					if nil != err {
						logger.Val("error", err).Error("next handler error")
					}
				}),

				rsocket.RequestResponse(func(request payload.Payload) mono.Mono {
					// Creo lo span opentelemetry
					ctx, span := c.createSpan(ctx, &request)
					defer span.End()

					// Invio l'evento al prossimo Handler
					outputEvent, err := bridge.Next(ctx, c.nextHandler, createEvent(request))
					if nil != err {
						logger.Val("error", err).Error("next handler error")
						return mono.Error(fmt.Errorf("internal server error"))
					}

					// Genero la respone
					return mono.Just(createPayload(outputEvent))
				}),
				rsocket.RequestStream(func(request payload.Payload) flux.Flux {
					return flux.Create(func(ctx context.Context, sink flux.Sink) {
						defer sink.Complete() // Segnale per indicare la fine dello stream

						// Creo lo span opentelemetry
						ctx, span := c.createSpan(ctx, &request)
						defer span.End()

						// Invio l'evento al prossimo Handler
						outputEvent, err := bridge.Next(ctx, c.nextHandler, createEvent(request))
						if nil != err {
							logger.Val("error", err).Error("next handler error")
							sink.Error(fmt.Errorf("internal server error"))
						}

						// Invio in dietro la risposta
						sink.Next(createPayload(outputEvent))
					})
				}),
				rsocket.RequestChannel(func(requests flux.Flux) flux.Flux {
					return flux.Create(func(ctx context.Context, sink flux.Sink) {
						requests.DoOnNext(func(request payload.Payload) error {
							defer sink.Complete() // Segnale per indicare la fine dello stream

							// Creo lo span opentelemetry
							ctx, span := c.createSpan(ctx, &request)
							defer span.End()

							// Invio l'evento al prossimo Handler
							outputEvent, err := bridge.Next(ctx, c.nextHandler, createEvent(request))
							if nil != err {
								logger.Val("error", err).Error("next handler error")
								sink.Error(fmt.Errorf("internal server error"))
							}

							// Invio in dietro la risposta
							sink.Next(createPayload(outputEvent))
							return nil
						}).DoOnComplete(func() {
							// Segnale di completamento del channel
							sink.Complete()
						}).Subscribe(context.Background())
					})
				}),
			), nil
		}).
		Transport(rsocket.TCPServer().SetAddr(fmt.Sprintf(":%d", c.port)).Build())

	// Avvio il server RSocket
	go func() {
		err := s.Serve(ctx)
		if nil != err {
			startedChan <- err
		}
	}()

	return <-startedChan
}

// Stop arresta il source
func (c *rSocketSource) Stop(ctx context.Context) error {
	return nil
}

// Ready indica se il source e' Ready
func (c *rSocketSource) Ready() bool {
	return c.ready > 0
}

// Type restituisce il tipo di source
func (c *rSocketSource) Type() string {
	return c.TypeSource
}

func createEvent(in payload.Payload) model.Event {
	// Conveto il messaggio in input in un Event
	return model.Event{
		Data: in.Data(), // Payload del messaggio
		// TODO gestire metadata
		//Metadata: in.Metadata(), // Metadati associati al messaggio
	}
}

func createPayload(event model.Event) payload.Payload {
	// TODO gestire metadata
	return payload.New(event.Data, nil)
}
