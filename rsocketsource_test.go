package rsocketsource

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
	"testing"

	"github.com/phayes/freeport"
	"github.com/rs/zerolog"
	"github.com/rsocket/rsocket-go"
	"github.com/rsocket/rsocket-go/payload"
	"github.com/rsocket/rsocket-go/rx"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/echohandler"
	"gotest.tools/assert"
)

func TestRSocketSource(t *testing.T) {
	// Canale su cui arrivera una notifica quando il Core e' ready
	chanReady := make(chan struct{})

	// Trovo una porta casuale libera che impostero come porta del server HTTP
	serverPort, err := freeport.GetFreePort()
	assert.NilError(t, err)

	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.DEBUG),
	)

	coreObj := core.NewCore(
		core.WithLogger(logger),
		core.WithChanReady(chanReady),
		core.WithSource("rsokect-source", New(
			"echo",
			WithPort(serverPort),
		)),
		core.WithHandler("echo", echohandler.New()),
	)

	go coreObj.Start()

	<-chanReady // Dopo questa riga il Core sara ready

	// Inizio a testare i vari messaggi da inviare al Server RSocket

	// Creo la connessione al server RSocket
	cli, err := rsocket.Connect().
		SetupPayload(payload.NewString("Hello", "World")).
		Transport(rsocket.TCPClient().SetHostAndPort("127.0.0.1", serverPort).Build()).
		Start(context.Background())
	assert.NilError(t, err)
	defer cli.Close()

	// ===> Invio "fire and forgot"
	cli.FireAndForget(payload.NewString("FireAndForgotData", "FireAndForgotMetadata"))

	// ===> Invio "MetadataPush" verra' inviato solo il metadata, non il data
	cli.MetadataPush(payload.NewString("dataCheNonverraInviato", "Metadata"))

	// ===> Invio "Request Response"
	result, err := cli.RequestResponse(payload.NewString("DataRequest", "MetadataRequest")).Block(context.Background())
	assert.NilError(t, err)

	metadata, _ := result.Metadata()
	assert.Equal(t, string(result.Data()), "DataRequest")
	//assert.Equal(t, ok, true)
	//assert.Equal(t, string(metadata), "MetadataRequest")
	log.Println("response:", string(result.Data()), " - ", string(metadata))

	// ===> Inzio "Request Stream"
	flux := cli.RequestStream(payload.NewString("DataRequestStream", "MetadataRequestStream"))

	wg := sync.WaitGroup{}
	wg.Add(1)

	flux.DoOnNext(func(input payload.Payload) error {
		// Stampo tutte i messaggi che arrivano
		fmt.Println(input.DataUTF8())
		return nil
	}).DoOnComplete(func() {
		fmt.Println("stream done")
	}).DoOnError(func(err error) {
		fmt.Println(err)
	}).DoFinally(func(s rx.SignalType) {
		wg.Done()
	}).Subscribe(context.Background())

	// Aspetto che lo stream finisca
	wg.Wait()

	// ===> Inzio "RequestChannel"
	// TODO
}
