package rsocketsource

// Option tipo funzione che rappresenta un opzione funzionale per inizializzare il natsSource
type Option func(*rSocketSource)

func WithPort(port int) Option {
	return func(c *rSocketSource) {
		c.port = port
	}
}
